/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.usermanamentproject;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class UserManamentProject {

    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "pass@1234", 'M', 'A');
        User user1 = new User("user1", "User 1", "pass@1234", 'F', 'U');
        User user2 = new User("user2", "User 2", "pass@1234", 'M', 'U');
        User user3 = new User("user3", "User 3", "pass@1234", 'F', 'U');

        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        System.out.println("Print for userArr");
        for (User a : userArr) {
            System.out.println(a);
        }

        ArrayList<User> userList = new ArrayList<>();
        userList.add(admin);
        System.out.println(userList.get(userList.size() - 1) + " List Size = " + userList.size());
        userList.add(user1);
        System.out.println(userList.get(userList.size() - 1) + " List Size = " + userList.size());
        userList.add(user2);
        System.out.println(userList.get(userList.size() - 1) + " List Size = " + userList.size());
        userList.add(user3);
        System.out.println(userList.get(userList.size() - 1) + " List Size = " + userList.size());

        System.out.println("Prin Arr");
        for (User a : userList) {
            System.out.println(a);
        }
        System.out.println("Print Set User");
        User user4 = new User("user4", "User 4", "pass@1234", 'M', 'U');
        userList.set(0,user4);
        userList.remove(userList.size()-1);
        for (User a : userList) {
            System.out.println(a);
        }

    }
}
